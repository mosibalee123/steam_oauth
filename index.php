<?php
    require ('steamauth/steamauth.php');  
	error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login With Steam by Jeny</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .table {
            table-layout: fixed;
            word-wrap: break-word;
        }
    </style>
  </head>
  <body style="background-color: #EEE;">
    <div class="container" style="margin-top: 30px; margin-bottom: 30px; padding-bottom: 10px; background-color: #FFF;">
		<h1>Login WIth Steam by Jenny</h1>
		<span class="small pull-left" style="padding-right: 10px;">for SteamAuth 3.2</span>
		<hr>
		<?php
if(!isset($_SESSION['steamid'])) {
    echo "<div style='margin: 30px auto; text-align: center;'>Welcome Guest! Please log in!<br>";
    loginbutton();
	echo "</div>";
	}  else {
    include ('steamauth/userInfo.php');
	?>	
		<span style='float:right;'><?php logoutbutton(); ?></span>
		<table class='table table-striped'>
			<tr>
				<td><b>Title</b></td>
				<td><b>Value</b></td>
				<td><b>Description</b></td>
			</tr>
			<tr>
				<td>Steam ID</td>
				<td><?=$steamprofile['steamid']?></td>
				<td>SteamID of the user</td>
			</tr>
			<tr>
				<td>Public Profile Name</td>
				<td><?=$steamprofile['personaname']?></td>
				<td>Public name of the user</td>
			</tr>
			<tr>
				<td>Profile Url</td>
				<td><?=$steamprofile['profileurl']?></td>
				<td>Link to the user's profile</td>
			</tr>
			<tr>
				<td>Profile Real Name</td>
				<td><?=$steamprofile['realname']?></td>
				<td>"Real" name</td>
			</tr>
			<tr>
				<td>Profile Avatar Image</td>
				<td>
					<img src='<?=$steamprofile['avatar']?>'><br>
					<?=$steamprofile['avatar']?>
				</td>
				<td>Address of the user's 32x32px avatar</td>
			</tr>
			<tr>
				<td>Profile Avatar Medium Image</td>
				<td>
					<img src='<?=$steamprofile['avatarmedium']?>'><br>
					<?=$steamprofile['avatarmedium']?>
				</td>
				<td>Address of the user's 64x64px avatar</td>
			</tr>
			<tr>
				<td>Profile Avatar Full Image</td>
				<td>
					<img src='<?=$steamprofile['avatarfull']?>'><br>
					<?=$steamprofile['avatarfull']?>
				</td>
				<td>Address of the user's 184x184px avatar</td>
			</tr>
		</table>
		<?php
		}    
		?>
		<hr>
	</div>
	<!--Version 4.0--> 
  </body>
</html>
